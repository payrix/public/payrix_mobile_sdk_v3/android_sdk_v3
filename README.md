# Payrix Android SDK Version 3.0 (EMV Certified)

## Release Note Summary:
Version 3.0.19
* Added transaction session (TxnSession) auth: This is going to be the alternative for Login Session.
* Supplementary Documentation [here](./sdkdocumentation/Payrix_Android_SDK_Dev.pdf)!

- Version 3.0.18
  * Android Mobile SDK Sandbox Decline Failure - Fix Amount 95.87 returning Approved
  * BT connection error issue for Android 12 and above bug fix.
  * Audio connection removed
  * Added more readable / user friendly error message

- Version 3.0.17
  * Added method doPhoneNFCDisable() to PayrixSDK class. This method helps to disable other NFC enabled app on the phone from responding when an NFC card reader is in proxy.

- Version 3.0.16
  * Exposed the Cancel Transaction function in SDK. Note: Cancel only works when the device is waiting for the card to be presented as a Tap, Chip Insert, or Swipe.  The SDK ignores all other Cancel requests once the card is presented.

- Version 3.0.15
  * Resolved the issue for ReverseAuth occurring when Payrix API does not handle Tag 8A length correctly.
  * Add OTA update steps on README file
  * Bullet proof PayrixSDK code from crashing and also added some data validation.

- Version 3.0.14
  * Enhanced bbPOS OTA deviceInfo response Data (new field "PayrixCurrentKeyProfileName")
  * Connecting to Incorrect Reader Occasionally bug fix
  * Manual, EMV and Swipe Refund cleanup

- Version 3.0.12
  * Chipper 3X BT connection error code 3020 fixed

- Version 3.0.11
  * Android 12 and above permission issue fixed

- Version 3.0.10
  * Card not Swiping fixed - EMV (Tap/Insert) takes more priority than SWIPE, for quick SWIPE response use cardDeviceMode_Swipe

- Version 3.0.9
  * Introduced scanning error message to method didReceiveScanResults of interface PayrixSDKCallbacks: If it's an error the method argument scanSuccess will be false, the scanMsg will be the error message and the PayDevice list will be empty
  * Added BBPOS OTA firmware update
  
- Version 3.0.8
  * Added a Compact BT scanning object for Android backward compatibility and support for Chipper 3x

- Version 3.0.7
  * Resolved manual transaction refund: Extra params are needed for a successful manual refund, this includes expiration, payment number and cvv
  * Return the name printed on card as ccName on PayResponse: This only works if BBPos SDK returns the name printed on card and the entryMode is set to Swipe

- Version 3.0.6
  * Added optional field 'order' to PayRequest class

- Version 3.0.5
  * New Methods introduced: doTransactionDataRequest with Callback: didReceiveGeneralTxnResponse (See SDK for details).
  * Resolved issue with Manual Payments
  * Resolved issue with Crash during lost Internet connection.
  * Resolved issue with Bluetooth locating and connecting to bbPOS Reader from a Samsung device. 
    This requires new Bluetooth permissions check in user application. Refer section below titled Bluetooth Access. 
  * Removed MANAGE_EXTERNAL_STORAGE from SDK Manifest

- Version 3.0.4
  * Fixed issue with Manual Entry Card Types.

- Version 3.0.2 - 3.0.3
  * This release of the Payrix Android SDK is completely restructured under a single framework that give the user full access to all of the services provided by previous SDK versions.
  * This is the first fully EMV certified release of the Payrix Android SDK, providing EMV card present payment capabilities of Chip Insert, and Contactless (Tap).
  Post Release Revisions:
    - Fixed Issues with PayDevices, PayMerchants being Singletons. Classes are no longer Singletons, and never needed to be.

- Version 3.0.0 - 3.0.1
  * This release of the Payrix Android SDK is completely restructured under a single framework that give the user full access to all of the services provided by previous SDK versions.
  * This is the first fully EMV certified release of the Payrix Android SDK, providing EMV card present payment capabilities of Chip Insert, and Contactless (Tap).
  Post Release Revisions:
    - Fixed BT Scan to properly add devices to returned PayDevices array.
    - Fixed DemoApp to use new SDK for BT scanning.
  
## Integrating the PayrixSDK into your Payment App

### 1.  Download this Payrix Mobile Android SDK Version 3.0 folder.

### 2.  After you unzip the download file you will find 3 files of interest:
    a.  This Developer’s Guide for Android.
    b.  The Payrix SDK folder
    c.  The PayrixSDK3Demo folder

### 3.  Adding the Payrix Android SDK to your App for the first time.
    a.  Please refer to the Payrix Android Developer's Guide

### 4.  Updating the Payrix SDK version in your App:
    a.  Open the Gradle (App) file
    b.  If you used a previous version of the SDK, 
        comment out (or delete) the specific "Implementation" statements for paycore and paycard,
        otherwise just comment out the payrixsdk "Implementation" statement.
    c.  Sync the Gradle
    d.  Switch the project from Android mode to Project mode.
    e.  If you used a previous version of the SDK, locate the payrixsdk (or paycore and paycard) .aar file and delete it.
    f.  Drag in the new payrixsdk.aar file into the same location
    g.  Switch the project back to Android mode from Project mode.
    h.  Open the Gradle (App) file again.
    i.  Uncomment the specific "Implementation" statement you previously commented out
        and Sync the Gradle
    j.  You can now proceed as normal with the compile and execution of the app.
    
    Note: the Android simulator is not supported in this release.

### 5.  You are now ready to use the new PayrixSDK in your App.


## Bluetooth Access

The latest versions of Android require additional Bluetooth permissions.  The following information is to assist in setting up those permissions.
The SDK does check that the permissions are granted before performing Bluetooth functions such as scanning and connecting to readers.

MANIFEST REVISIONS:

    <uses-permission android:name="android.permission.BLUETOOTH" />
    <uses-permission android:name="android.permission.BLUETOOTH_ADMIN" />
    <uses-permission android:name="android.permission.BLUETOOTH_SCAN" />
    <uses-permission android:name="android.permission.BLUETOOTH_CONNECT" />

USER CODE REVISIONS:

// Handling permissions status and requesting access

	   if ((ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                    || (ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH) != PackageManager.PERMISSION_GRANTED)
                    || (ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_ADMIN) != PackageManager.PERMISSION_GRANTED)
                    || (ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_SCAN) != PackageManager.PERMISSION_GRANTED)
                    || (ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_CONNECT) != PackageManager.PERMISSION_GRANTED)
                    || (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                    || (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                    || (ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED)
                    || (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED))
            {
                // Permission is not granted, request access

                String[] permissionList = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, 
							Manifest.permission.BLUETOOTH, 
							Manifest.permission.BLUETOOTH_ADMIN, 
							Manifest.permission.BLUETOOTH_SCAN, 
							Manifest.permission.BLUETOOTH_CONNECT, 
							Manifest.permission.ACCESS_COARSE_LOCATION, 
							Manifest.permission.ACCESS_FINE_LOCATION, 
							Manifest.permission.INTERNET, 
							Manifest.permission.ACCESS_NETWORK_STATE};

                ActivityCompat.requestPermissions(this, permissionList, PERMISSION_REQUEST_CODE);
            }
            else
            {
                // Permissions granted, proceed
            }

// Handling Permissions Response by User

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CODE)
        {
            // If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                // permission was granted, do your work....
            }
            else
            {
                // permission denied display warning to user.
            }
        }
    }

// Before performing an SDK Bluetooth request check the Bluetooth Permissions

    protected boolean checkBluetoothPermission()
    {
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH) == PackageManager.PERMISSION_GRANTED) &&
                (ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_SCAN) == PackageManager.PERMISSION_GRANTED) &&
                (ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_CONNECT) == PackageManager.PERMISSION_GRANTED) &&
                (ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_ADMIN) == PackageManager.PERMISSION_GRANTED))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

All of the above sample code is in the latest version of the DemoApp provided with the SDK.


## SDK Process Map of SDK Functions (Methods and Callbacks)

![](sdkdocumentation/PayrixSDK_Process_Map_Pg1.png)

![](sdkdocumentation/PayrixSDK_Process_Map_Pg2.png)

![](sdkdocumentation/PayrixSDK_Process_Map_Pg3.png)

![](sdkdocumentation/PayrixSDK_Process_Map_Pg4.png)

![](sdkdocumentation/PayrixSDK_Process_Map_Pg5.png)
